#!/bin/bash


if [ ! -d "/opt/jdk1.8.0_202/" ];then
    echo "华为云下载oracle jdk8"
    curl -o jdk-linux-x64.tar.gz  https://mirrors.huaweicloud.com/java/jdk/8u202-b08/jdk-8u202-linux-x64.tar.gz
    echo "安装oracle jdk8"
    sudo tar -zxvf jdk-linux-x64.tar.gz -C /opt
    sudo echo 'export JAVA_HOME=/opt/jdk1.8.0_202' >> /etc/profile
    sudo echo 'export JRE_HOME=${JAVA_HOME}/jre' >> /etc/profile
    sudo echo 'export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib'  >> /etc/profile
    sudo echo 'export PATH=.:${JAVA_HOME}/bin:$PATH' >> /etc/profile
    source /etc/profile
  else
    echo ""
fi

sudo rm -rf jdk-linux-x64.tar.gz
echo "java -version"
java -version

