#!/bin/bash

sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
sudo yum makecache fast
sudo yum -y install docker-ce
echo "docker下载安装完毕"
echo "#***************************************************#"
sudo systemctl start docker
echo "#***************************************************#"
echo "docker设置开机启动"
systemctl enable docker
echo "#***************************************************#"

echo "#***************************************************#"
echo "docker 镜像源配置阿里云"
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://yyfojse0.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker