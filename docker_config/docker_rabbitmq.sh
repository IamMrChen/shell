#!/bin/bash

echo "docker 安装RabbitMq 端口 5672 ,网页端口 15672"
echo "账号:guest, 密码:guest"
docker run -d --name rabbitmq \
-p 15672:15672              \
-p 5672:5672                \
rabbitmq:3.8-management

echo "docker 安装RabbitMq 配置自启"
sudo docker update --restart=always rabbitmq
