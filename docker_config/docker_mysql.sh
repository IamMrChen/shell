#!/bin/bash

echo "docker 安装mysql 端口3306, 账号:root, 密码:123456"
sudo docker run -p 3306:3306 --name mysql \
-v /usr/local/docker/mysql/conf:/etc/mysql \
-v /usr/local/docker/mysql/logs:/var/log/mysql \
-v /usr/local/docker/mysql/data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=123456 \
-d mysql:5.7
echo "docker mysql 配置自启"
sudo docker update --restart=always mysql
