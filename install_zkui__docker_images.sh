#!/bin/bash
# 创建一个zkui的镜像，需要和zookeeper的链接地址配置为： ZK_SERVER=zookeeper

# 安装mvn
sh install_mvn.sh

#下载zk源码
git clone https://github.com/DeemOpen/zkui.git
cd zkui/

mvn clean install
cp config.cfg docker
cp target/zkui-*-jar-with-dependencies.jar docker
docker build -t zkui:2.0.0 --no-cache --rm docker
rm docker/zkui-*.jar
rm docker/config.cfg
docker build -t zkui:2.0.0 --no-cache --rm docker