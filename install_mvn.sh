#!/bin/bash

echo "安装mvn前，先配置jdk"
sh install_jdk8.sh

if [ ! -d "/opt/maven/" ];then
    echo "下载mvn"
    wget http://mirrors.hust.edu.cn/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
    tar -zxvf  apache-maven-3.6.3-bin.tar.gz -C  /opt
    mv /opt/apache-maven-3.6.3 /opt/maven
    sudo echo 'export MAVEN_HOME=/opt/maven' >> /etc/profile
    sudo echo 'export PATH=$MAVEN_HOME/bin:$PATH' >> /etc/profile
    source /etc/profile
  else
    echo ""
fi
echo "mvn -version"
mvn -version

